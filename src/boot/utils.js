import { date, Notify } from 'quasar'

export default ({ Vue, store, route }) => {
  Vue.prototype.$utils = {
    formatMoney: (value) => {
      return `${(1.0 * value).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$& ')}₽`
    },
    formatDate: (value) => date.formatDate(value, 'DD.MM.YYYYг'),
    daysBetweenNow: (value) => date.getDateDiff(value, new Date(), 'days'),
    handleAxiosError: (error) => {
      if (error.response && error.response.data && error.response.data.message) {
        Notify.create({ message: error.response.data.message })
      } else {
        Notify.create({ message: error.message })
      }
    }
  }
}
