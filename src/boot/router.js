export default async ({ router, store }) => {
  router.beforeEach(async (to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      const { state: { auth: { accessToken } } } = store
      if (!accessToken) {
        try {
          await store.dispatch('auth/refreshAccessToken')
          next({ name: to.name })
        } catch {
          if (from.name !== 'login') {
            next({
              name: 'login',
              query: { redirect: to.fullPath }
            })
          }
        }
      } else {
        next()
      }
    } else if (to.matched.some(x => x.name === 'login')) {
      console.error(to.name)
      try {
        await store.dispatch('auth/refreshAccessToken')
        next({ name: 'dashboard' })
      } catch {
        next()
      }
    } else {
      next()
    }
  })

  router.onError(async (error, vm, info) => {
    console.error(error)
  })
}
