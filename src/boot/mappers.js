export default ({ Vue, store, route }) => {
  Vue.prototype.$mappers = {
    mapTicketTemplateDepartment (deparment) {
      switch (deparment) {
        case 'bookkeeping':
          return 'Обращение в бухгалтерию'
        case 'security':
          return 'Обращение в службу безопасности'
      }
    },
    mapTicketTemplateName (name) {
      switch (name) {
        case 'tax-reference':
          return 'Справка для налоговой'
        case 'delay':
          return 'Отсрочка'
      }
    },
    mapTicketTemplateNameDescription (name) {
      switch (name) {
        case 'tax-reference':
          return 'Заявка на выдачу справки для налоговой и тд и тп.\nДействительна в течение 14 календарных дней.\nСрок изготовления до 2 рабочих дней.'
        case 'delay':
          return 'Заявка на отсрочку платежа.\nЗакажите звонок от специалиста СБ для одобрения отсрочки платежа по займу.'
      }
    },
    mapTicketStatus (status) {
      switch (status) {
        case 'new':
          return 'В рассмотрении'
        case 'progress':
          return ''
      }
    }
  }
}
