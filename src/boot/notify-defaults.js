import { Notify } from 'quasar'

Notify.setDefaults({
  position: 'bottom',
  timeout: 2500,
  textColor: 'white',
  actions: [{ icon: 'o_close', color: 'white' }]
})
