import VueMask from 'v-mask'

export default async ({ Vue }) => {
  Vue.use(VueMask)
}
