import axios from 'axios'

const baseURL = 'https://api.ekpa.ru:3000'
const withCredentials = true

const axiosInstanceUnauthenticated = axios.create({ baseURL, withCredentials })
const axiosInstanceAuthenticated = axios.create({ baseURL, withCredentials })

export default ({ Vue, store, route, router }) => {
  const createAxiosRequestInterceptor = configuration => {
    configuration.headers['Access-Control-Allow-Credentials'] = true
    if (store.state.auth.accessToken) {
      configuration.headers.Authorization = 'Bearer ' + store.state.auth.accessToken
    } else {
      delete configuration.headers.Authorization
    }
    return configuration
  }

  axiosInstanceUnauthenticated.interceptors.request.use(createAxiosRequestInterceptor)
  axiosInstanceAuthenticated.interceptors.request.use(createAxiosRequestInterceptor)

  const createAxiosResponseInterceptor = () => {
    const interceptor = axiosInstanceAuthenticated.interceptors.response.use(
      response => response,
      error => {
        if (error.response.status === 401) {
          axiosInstanceAuthenticated.interceptors.response.eject(interceptor)
          return store.dispatch('auth/refreshAccessToken')
            .then(() => axiosInstanceAuthenticated)
            .catch((error) => Promise.reject(error))
            .finally(createAxiosResponseInterceptor)
        } else if (error.response.status === 420) {
          router.replace({ name: 'agreement' })
          return Promise.reject(error.response.data.message)
        }
        return Promise.reject(error)
      }
    )
  }

  store.subscribe((mutation, state) => {
    console.log(mutation.type, mutation.payload)
  })

  createAxiosResponseInterceptor()
  Vue.prototype.$axiosAuthenticated = axiosInstanceAuthenticated
  Vue.prototype.$axiosUnauthenticated = axiosInstanceUnauthenticated
  store.$axiosAuthenticated = axiosInstanceAuthenticated
  store.$axiosUnauthenticated = axiosInstanceUnauthenticated
}

export { axiosInstanceAuthenticated, axiosInstanceUnauthenticated }
