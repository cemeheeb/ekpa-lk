import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import member from './modules/member'

Vue.use(Vuex)

export default function () {
  const store = new Vuex.Store({
    modules: {
      auth,
      member
    },
    strict: process.env.DEV
  })

  return store
}
