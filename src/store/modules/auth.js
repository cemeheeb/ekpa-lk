import { Notify } from 'quasar'

const state = () => ({
  accessToken: null
})

const handleError = (error) => {
  if (error.response && error.response.data && error.response.data.message) {
    Notify.create({ message: error.response.data.message })
  } else {
    Notify.create({ message: error.message })
  }
}

const actions = {
  async sendVerificationCode ({ commit }, { id, phone }) {
    try {
      await this.$axiosUnauthenticated.post(
        'auth/phone-send-verification-code',
        { id, phone }
      )
    } catch (error) {
      handleError(error)
      return Promise.reject(error)
    }
  },
  async resendVerificationCode ({ commit }, { id, phone }) {
    try {
      await this.$axiosUnauthenticated.post(
        'auth/phone-resend-verification-code',
        { id, phone }
      )
    } catch (error) {
      handleError(error)
      return Promise.reject(error)
    }
  },
  async acceptAgreements ({ commit, state, dispatch }) {
    try {
      const {
        data: {
          accessToken
        }
      } = await this.$axiosAuthenticated.post('auth/refresh-token')
      commit('setAccessToken', accessToken)
      await this.$axiosAuthenticated.post('agreement')
      this.$router.replace({ name: 'dashboard' })
    } catch (error) {
      handleError(error)
      return Promise.reject(error)
    }
  },
  async login ({ commit }, { phone, phoneVerificationCode }) {
    try {
      await this.$axiosUnauthenticated.post(
        'auth/login',
        { phone, phoneVerificationCode }
      )
    } catch (error) {
      handleError(error)
      return Promise.reject(error)
    }
  },
  async refreshAccessToken ({ commit }) {
    const {
      data: {
        accessToken
      }
    } = await this.$axiosAuthenticated.post('auth/refresh-token')
    commit('setAccessToken', accessToken)
  },
  async logout ({ commit }) {
    await this.$axiosAuthenticated.post('auth/logout')
    this.$router.replace({ name: 'login' })
    commit('deleteAccessToken')
  }
}

const mutations = {
  setAccessToken (state, accessToken) {
    state.accessToken = accessToken
  },
  deleteAccessToken (state) {
    state.accessToken = null
  },
  acceptAgreementCookies (state) {
    state.acceptAgreementCookies = true
  },
  acceptAgreementInternal (state) {
    state.acceptAgreementInternal = true
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
