import moment from 'moment'

const state = () => ({
  firstName: '-',
  displayName: '-',
  joinDate: '-',
  loan: {
    collection: []
  },
  guarantee: {
    collection: []
  },
  deposit: {
    collection: []
  },
  share: {
    collection: []
  },
  notification: {
    collection: []
  }
})

const actions = {
  async refresh ({ commit }) {
    const {
      data: {
        displayName,
        firstName,
        joinDate,
        acceptedNotifications,
        acceptedAgreement,
        __loans__,
        __guaranties__,
        __deposits__,
        __shares__,
        __notifications__
      }
    } = await this.$axiosAuthenticated.get('member')
    commit('initialize', {
      displayName,
      firstName,
      joinDate,
      acceptedNotifications,
      acceptedAgreement,
      loans: __loans__,
      guaranties: __guaranties__,
      deposits: __deposits__,
      shares: __shares__,
      notifications: __notifications__
    })
  }
}

const mutations = {
  initialize (state, {
    displayName,
    firstName,
    joinDate,
    acceptedNotifications,
    acceptedAgreement,
    loans,
    guaranties,
    deposits,
    notifications
  }) {
    state.displayName = displayName
    state.firstName = firstName
    state.joinDate = moment(joinDate, 'YYYY-MM-DD').format('DD.MM.YYYY')
    state.ticketTemplates = [
      {
        name: 'tax-reference',
        displayName: 'Справка для налоговой',
        department: 'bookkeeping'
      },
      {
        name: 'delay',
        displayName: 'Отсрочка',
        department: 'security'
      }
    ]
    state.acceptedNotifications = acceptedNotifications
    state.acceptedAgreement = acceptedAgreement
    state.loans = loans.map((x) => {
      const {
        __member__,
        remains,
        __guarantors__,
        __shares__,
        __pledges__,
        ...rest
      } = x
      return {
        ...rest,
        member: __member__,
        remains: parseFloat(x.remains),
        guarantors: __guarantors__,
        shares: __shares__,
        pledges: __pledges__
      }
    })
    state.guaranties = guaranties.map((x) => {
      const {
        __member__,
        remains,
        __guarantors__,
        __pledges__,
        ...rest
      } = x
      return {
        ...rest,
        member: __member__,
        remains: parseFloat(x.remains),
        guarantors: __guarantors__,
        pledges: __pledges__
      }
    })
    state.deposits = deposits.map((x) => {
      const {
        __shares__,
        ...rest
      } = x
      return {
        ...rest,
        shares: __shares__
      }
    })
    state.notifications = notifications
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
