export default [
  {
    path: '/',
    redirect: { name: 'dashboard' },
    component: () => import('layouts/layout-authenticated.vue'),
    children: [
      {
        path: 'dashboard',
        name: 'dashboard',
        component: () => import('pages/page-dashboard.vue')
      },
      {
        path: 'deposits',
        name: 'deposit',
        component: () => import('pages/page-deposit.vue')
      },
      {
        path: 'loans',
        name: 'loan',
        component: () => import('pages/page-loan.vue')
      },
      {
        path: 'guaranties',
        name: 'guarantee',
        component: () => import('pages/page-guarantee.vue')
      }
      // {
      //   path: 'notifications',
      //   name: 'notification',
      //   component: () => import('pages/page-notification.vue')
      // }
      // {
      //   path: 'tickets',
      //   name: 'ticket',
      //   component: () => import('pages/page-ticket.vue')
      // }
    ],
    meta: { requiresAuth: true }
  },
  {
    path: '/login',
    component: () => import('layouts/layout-blank.vue'),
    children: [
      {
        path: '',
        name: 'login',
        component: () => import('pages/page-login.vue')
      }
    ]
  },
  {
    path: '/404',
    component: () => import('layouts/layout-blank.vue'),
    children: [
      {
        path: '',
        name: 'error-not-found',
        component: () => import('pages/page-error-not-found.vue')
      }
    ]
  },
  {
    path: '/agreement',
    component: () => import('layouts/layout-blank.vue'),
    children: [{
      path: '',
      name: 'agreement',
      component: () => import('pages/page-agreement.vue')
    }]
  },
  {
    path: '*',
    redirect: { name: 'error-not-found' }
  }
]
